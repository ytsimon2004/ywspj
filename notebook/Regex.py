#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re


# `.`  - **Any Character Except New line <br>**
# `\d`  - **Digit(0-9) <br>**
# `\D`  - **Not a Digit(0-9) <br>**
# `\w`  - **Word Character(a-z, A-Z 0-9, _) <br>**
# `\W`  - **Not a word character <br>**
# `\s`  - **Whitespace(space, tab, newline) <br>**
# `\S`  - **Not a Whitespace(0-9) <br>**
# `\b`  - **Word boundary <br>**
# `\B`  - **Not a word boundary <br>**
# 
# `\^`  - **Begining of a string <br>**
# `\$`  -**End of a string <br>**

# In[28]:


s = 'lolabc''.123'


pattern = re.compile(r'abc')
matches = pattern.finditer(s)

for i in matches:
    print(i)  # span as an index 


# ### MetaCharacters (Need to be escaped):
# **`.^$*+?{}[]\|()`**

# In[34]:


pattern = re.compile(r'\.')  # escape for searching .
matches = pattern.finditer(s)

for i in matches:
    print(i)


# In[ ]:




