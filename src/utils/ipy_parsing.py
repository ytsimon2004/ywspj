# jnv.py: A simple viewer of a Jupyter notebooks (ipynb-files).
# Works for nbformat version >= 4.
import tkinter as tk
import sys, json

f = open(sys.argv[1], 'r', encoding="utf8")  # input.ipynb
jf = json.load(f)
f.close()

# Take text ('source') from 'markdown' and 'code' cells
out_txt = ''
for cell in jf["cells"]:
    if cell['cell_type'] == 'markdown':
        for el in cell['source']:
            out_txt = out_txt + el
    elif cell['cell_type'] == 'code':
        for el in cell['source']:
            out_txt = out_txt + el

# Make a frame and display 'out_txt'. Press Esc to quit.
# See https://www.python-course.eu/tkinter_text_widget.php
root = tk.Tk()

S = tk.Scrollbar(root)
T = tk.Text(root, height=24, width=80)


def select_all(event=None):
    T.tag_add('sel', '1.0', 'end')
    # return "break"


def copy_sel(event=None):
    content = T.selection_get()
    print(content)
    root.clipboard_clear()
    root.clipboard_append(content)


def key(event):
    print(event)
    if event.keycode == 27:  # pressed Esc
        root.destroy()
    elif event.char == '\x01':  # Ctrl-A; make sure you use this before cursor enters text!
        select_all()
    elif event.char == '\x03':  # Ctrl-C; make sure you use this before cursor enters text!
        copy_sel()


S.pack(side=tk.RIGHT, fill=tk.Y)
T.pack(side=tk.LEFT, fill=tk.Y)
S.config(command=T.yview)
T.config(yscrollcommand=S.set)

T.insert(tk.END, out_txt)
root.bind("<Key>", key)
tk.mainloop()
