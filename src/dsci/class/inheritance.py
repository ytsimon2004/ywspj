from typing import Optional, List

from method import *


__all__ = ['Developer',
           'Manager']

class Developer(Employee):
    raise_amt = 1.5  # change the class variables

    def __init__(self, firstname: str, lastname: str, pay: int, prog_lang: str):
        super().__init__(firstname, lastname, pay)
        self.prog_lang = prog_lang


class Manager(Employee):
    def __init__(self, firstname: str, lastname: str, pay: int, employee: Optional[List[Employee]]):
        super().__init__(firstname, lastname, pay)

        self.employee = employee if employee is not None else []

    def emp_fullname(self) -> List[str]:
        return [it.fullname() for it in self.employee]

    def n_emp(self) -> int:
        return len(self.employee)

    def add_emp(self, emp: str):

        if emp not in self.emp_fullname():
            self.employee.append(Employee.name_from_str(emp))  # use constructor
        else:
            raise ValueError(f'{emp} already exist in the emp list')

    def remove_emp(self, emp: str):
        if emp in self.emp_fullname():
            for i, it in enumerate(self.emp_fullname()):
                if emp == it:
                    self.employee.pop(i)
        else:
            raise ValueError(f'{emp} not in the emp list')


########

"""super init"""
# D = Developer(firstname='yu-ting', lastname='Wei', pay=1000, prog_lang='Python')
# print(D.email)


"""Manager"""
# emp1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# emp2 = Employee(firstname='Vincent', lastname='bonin', pay=200)
# m = Manager(firstname='Fabian', lastname='kloosterman', pay=300, employee=[emp1, emp2])
#
# print(m.emp_fullname())
# print(m.n_emp())
#
# m.add_emp('ta-shun Su')
# print(m.emp_fullname())
# print(m.n_emp())
#
# m.remove_emp('ta-shun Su')
# print(m.emp_fullname())
# print(m.n_emp())
#
# m.remove_emp('Vincent bonin')
# print(m.emp_fullname())
# print(m.n_emp())
