from typing import Tuple, Optional, List, Union
import datetime

__all__ = ['Employee']

import numpy as np


class Employee:
    # class variables
    n_emp = 0
    raise_amt = 1.04
    lang: List[str] = ['python', 'java', 'matlab']

    def __init__(self, firstname: str, lastname: str, pay: float):
        # instance variables, attributes
        self.first = firstname
        self.last = lastname
        self.pay = pay

        self.email = f'{firstname}_{lastname}@gmail.com'


    def fullname(self) -> str:
        return f'{self.first} {self.last}'

    def apply_raise(self) -> float:
        return self.pay * self.raise_amt

    @classmethod
    def set_raise_amt(cls, new_amt: float):
        """
        call method without init and not use instance varible

        **Example**

        >>> Employee.set_raise_amt(1.5)

        >>> e = Employee(firstname=..., lastname=..., ...)
        >>> e.raise_amt
        >>> 1.5

        """
        cls.raise_amt = new_amt

    @classmethod
    def name_from_str(cls, s: str) -> 'Employee':
        """
        use as a constructor

        **Example**

        >>> e = Employee.name_from_str('yu-ting wei')
        >>> e.fullname()
        >>> 'yu-ting wei'

        """
        first, last = s.split(' ')
        return cls(first, last, 100)

    @staticmethod
    def is_work_day(date: datetime) -> bool:
        """
        do not use class or instance variables

        **Example**

        >>> d = datetime.date(2022, 3, 2)
        >>> Employee.is_work_day(d)
        >>> True
        """
        if date.weekday() in (5, 6):
            return False
        return True

    def __str__(self):
        """ give a user friendly representation"""
        return f'{self.fullname()}'

    # __repr__ = __str__

    def __repr__(self):
        """give developer-friendly representation"""
        return f'{self.fullname()}, {self.pay}, {self.email}'

    def __add__(self, other):
        """add pay between cls obj"""
        if isinstance(other, Employee):
            return self.pay + other.pay
        else:
            raise TypeError('')

    def __len__(self):
        """number of letters in fullname"""
        let = ''
        for i in self.fullname():
            if i.isalpha():
                let += i

        return len(let)

    def __eq__(self, other):
        """equal pay or not"""
        if not isinstance(other, Employee):
            raise TypeError('')
        else:
            return self.pay == other.pay

    def __getitem__(self, item):
        """get last and first name"""
        if item > 2:
            raise ValueError('')
        return self.fullname().split(' ')[item]

    def __setitem__(self, key, value):
        """set lang list"""
        self.lang[key] = value

    def __contains__(self, item):
        if item in self.lang:
            return True
        return False

    def __call__(self, *args, **kwargs):
        pass
    # todo



########

"""init"""
# e = Employee(firstname='yu-ting', lastname='wei', pay=100)
# print(e.fullname())

"""call method without init"""
# Employee.set_raise_amt(new_amt=1000)
# print(Employee.raise_amt)


"""cls method use for constructor"""
# e = Employee.name_from_str('yu-ting wei')
# print(e.fullname())


"""do not use class or instance variables"""
# d = datetime.date(2022, 3, 2)
# print(d.weekday())
# print(Employee.is_work_day(d))


"""str, repr"""
# e = Employee(firstname='yu-ting', lastname='wei', pay=100)
# print(e)
# print(repr(e))

"""add"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# e2 = Employee(None, None, pay=200)
# print(e1+e2)

"""add"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# print(len(e1))

"""eq"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# e2 = Employee(None, None, pay=100)
# print(e1 == e2)

"""get"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# print(e1[1])

"""set"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# e1[0] = 'python3.0'
# print(e1.lang)

"""contain"""
# e1 = Employee(firstname='yu-ting', lastname='wei', pay=100)
# if 'python' in e1.lang:
#     print('well done!')
