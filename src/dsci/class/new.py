class A:

    def __new__(cls, *args, **kwargs):
        """new an object"""
        print('new', cls, args, kwargs)
        return super().__new__(cls)

    def __init__(self, *args, **kwargs):
        """if the type correct, init the argument"""
        print('init', self, args, kwargs)


def ex1():
    # x = A(1, 2, 3, x=4)
    x = A.__new__(A, 1, 2, 3, x=4)
    if isinstance(x, A):
        type(x).__init__(x, 1, 2, 3, x=4)
    print(x)


# ======

class UpperCase(tuple):

    def __new__(cls, words: tuple):
        print('new...')
        upper = (w.upper() for w in words)
        return super().__new__(cls, upper)

    def __init__(self, word):
        print('init..')
        print(word)
        self.word = word


def ex2():
    words = ('i', 'am', 'handsome')
    u = UpperCase(words)
    print(u)


class Singleton:  # eg global config object
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance


if __name__ == '__main__':
    a = Singleton()
