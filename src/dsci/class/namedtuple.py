from typing import NamedTuple, Any


class Test(NamedTuple):
    a: int
    b: int
    c: int


class Outer(NamedTuple):
    d: int
    e: Any


def x(a, b, c):
    return Test(a, b, c)


def y(d, e):
    return Outer(d, Test(d, e, d + e))


#
# T = x(2, 3, 4)
# print(T.a)

O = y(6, 2)
print(O.e[2])
