from typing import Optional, List

from .method import Employee
from .inheritance import Manager


class Sponser(Manager):
    def __init__(self, firstname, lastname, pay, manager: Optional[List[Manager]] = None):
        super().__init__(firstname, lastname, pay, employee=None)
        self.manager = manager if manager is not None else []

    @property
    def n_mang(self):
        """define a method, but access as an attribute"""
        return len(self.manager)

    @property
    def s_fullname(self) -> str:
        return self.fullname()

    @s_fullname.setter
    def s_fullname(self, name):
        """only use in property"""
        first, last = name.split(' ')
        self.first = first
        self.last = last

    @s_fullname.deleter
    def s_fullname(self):
        print('delete fullname')
        self.first = None
        self.last = None


if __name__ == '__main__':
    m1 = Manager(firstname='Fabian', lastname='kloosterman', pay=300, employee=None)
    m2 = Manager(firstname='Vincent', lastname='bonin', pay=200, employee=None)

    s = Sponser(firstname='imec', lastname='head', pay=1000, manager=[m1, m2])

    """use 'Employee method'"""
    # e = s.name_from_str(s='bonbon cat')
    # print(e.fullname())

    # print(s.email)

    """no property decorator"""
    # ret = s.n_mang()
    # print(ret)

    """with property"""
    # print(s.n_mang)


    """setter"""
    # print(s.s_fullname)
    # s.s_fullname = 'bonbon cat'
    # print(s.s_fullname)

    """deleter"""
    s.s_fullname = 'bonbon cat'
    print(s.s_fullname)
    del s.s_fullname
    print(s.s_fullname)
    print(s.first)