from typing import overload, Union


class Person:

    def __init__(self, age, income):
        self.age: str = age
        self.income: float = income

    @overload
    def show(self, age: str):
        pass


    @overload
    def show(self, income: float):
        pass


    def show(self, input: Union[str, float]):
        if isinstance(input, str):
            print(f'age is {self.age}')
        elif isinstance(input, (float, int)):
            print(f'income is {self.income}')
        else:
            raise TypeError('')


if __name__ == '__main__':

    p = Person('15', 1000)
    p.show('15')


