import numpy as np
from io import StringIO

"""
**lambda**
syntax: lambda argument: expression
"""

# f = lambda a, b, c: print(a + b + c)
# f(1, 2, 3)

# -----

# # mimic problematic file obj
# fname = StringIO('[1,2,3,4.7,5]')
#
# log = np.loadtxt(fname=fname, delimiter=',',
#                  converters={
#                      0: lambda it: float(it[1:]),
#                      3: lambda it: float(it[:-2]),
#                      4: lambda it: float(it[:-1])
#                  })
# print(log)


"""
**eval**
eval(expression)
"""

# print(eval('3-4-5'))
#
# # -----
# x = "{'phase':['linear',1]}"
# # extract the value from str[Dict[str,[str,int]]], used in prot file parsing
# print(eval(x)['phase'][1])


"""
**map**
syntax: map(func, iter) 
"""

# def my_sq(x):
#     return x * x
#
#
# a = [1, 2, 3]
# print(list(map(my_sq, a)))

# ----

# # combine with lambda
# a = [1, 2, 3]
# print(list(map(lambda it: it * it, a)))

# ---

# # Convert all elements of list in uppercase
# ls = ['simon', 'is', 'cool']
# print(list(map(lambda it: it.upper(), ls)))

# ----

# # int_tuple_type
# s = '10,20,30'
# print(tuple(map(int, s.split(','))))


"""
zip
syntax: zip(*iterables) -> Tuple
"""
# name = ['simon', 'vincent', 'fabian']
# age = (25, 50, 40)
# weight = [75, 90, 65]
#
# print(list(zip(name, age, weight)))
# # create dict
# print(dict(zip(name, age)))

# -----

# moving window filter
x = [1, 2, 3, 4, 5, 6]
mean_x = []
for (l, r) in zip(x[:-1], x[1:]):
    mean_x.append(np.mean((l, r)))

print(mean_x)