import abc


# 繼承 封裝 多型

class Animal:

    def __init__(self):
        self.name = f'{type(self).__name__}'
        self.__zoo = 'Taipei'  # private (superprivate)
        self._country = 'Taiwan'  # internal use (semiprivate)

    def poo(self):
        print('poooo')

    @abc.abstractmethod
    def make_sound(self) -> str:
        pass


class Pig(Animal):

    def make_sound(self) -> str:
        return f'{self.name} is making ohohh'


class Cat(Animal):

    def make_sound(self) -> str:
        return f'{self.name} is making mimimi'


def main(animal: Animal, test: str):
    if test == 'inheritance':
        animal.poo()
    elif test == 'polymorphism':
        print(animal.make_sound())
    elif test == 'encapsulate':
        print(animal._country)  # accessible
        print(animal.__zoo)  # err
    else:
        raise ValueError('')


if __name__ == '__main__':
    main(Pig(), 'polymorphism')
    main(Cat(), 'polymorphism')
