import argparse
import re
from datetime import datetime
from typing import NamedTuple, Optional, List

import instaloader
from rich.pretty import pprint
import emoji


class SearchResult(NamedTuple):
    """see instaloader.structure.Post"""
    date_local: datetime.date
    username: str
    caption: Optional[str]
    likes: int

    def to_pickle(self):
        """save object as pickle file TODO"""
        pass


def clean_caption(caption: str) -> str:
    # Remove line breaks and extra spaces
    cleaned_caption = re.sub(r'\s+', ' ', caption)
    # Remove special characters
    cleaned_caption = re.sub(r'[^\w\s#@]', '', cleaned_caption)
    return cleaned_caption

def search_hashtag(hashtag: str) -> List[SearchResult]:
    loader = instaloader.Instaloader()

    ret = []
    for post in loader.get_hashtag_posts(hashtag):
        date_local = post.date_local.date()
        username = post.owner_profile.username
        caption = clean_caption(emoji.demojize(post.caption))
        likes = post.likes
        result = SearchResult(date_local, username, caption, likes)
        pprint(result)
        ret.append(result)

    return ret

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument(metavar='TAG', help='hashtag', dest='hashtag')

    opt = ap.parse_args()

    search_hashtag(opt.hashtag)



if __name__ == '__main__':
    main()
