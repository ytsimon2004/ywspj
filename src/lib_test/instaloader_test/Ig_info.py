import json
import re
from copy import deepcopy
from pathlib import Path
from typing import TypedDict, Any, NamedTuple
import polars as pl


class PostDict(TypedDict):
    id: str
    type: str
    shortCode: str
    caption: str
    hashtags: list[str]
    mentions: list[str]
    url: str
    commentsCount: int
    firstComment: str
    latestComments: list[Any]  # TODO check
    dimensionsHeight: int
    dimensionsWidth: int
    displayUrl: str
    images: list[Any]  # TODO check
    alt: Any | None  # TODO check
    likesCount: int
    timestamp: str
    childPosts: list[Any]  # TODO check
    ownerId: str


class DownloadDict(TypedDict):
    id: str
    name: str
    url: str
    topPostsOnly: bool
    profilePicUrl: str
    postsCount: int
    topPosts: list[PostDict | None]
    latestPosts: list[PostDict | None]


class LatestPostInfo(NamedTuple):
    hashtag: str | list[str]
    data: list[PostDict]

    @property
    def n_posts(self) -> int:
        return len(self.data)

    def contain_duplicated(self, field: str = 'id') -> bool:
        validate = set()
        for it in self.data:
            validate.add(it[field])
        return len(self.data) != len(validate)

    def remove_duplicate(self):
        # TODO
        pass

    def to_dataframe(self) -> pl.DataFrame:
        try:
            return pl.DataFrame(self.data)
        except pl.ComputeError:
            return pl.DataFrame(self.data, infer_schema_length=300)

    @classmethod
    def concat(cls, infos: list['LatestPostInfo']) -> 'LatestPostInfo':
        hashtags = []
        batch_data = []
        for info in infos:
            hashtags.append(info.hashtag)
            batch_data.extend(info.data)

        return LatestPostInfo(hashtags, batch_data)

    def download_image(self, output_dir: Path | str):
        """download image with `ID` filename TODO"""
        pass

    def to_pickle(self):
        pass


class IgInfoFactory:

    def __init__(self, file: str,
                 data: list[DownloadDict]):

        self._file = file
        self.data = data

    @classmethod
    def load(cls, file: Path | str) -> 'IgInfoFactory':
        file = Path(file)
        if file.exists() and file.is_file():
            with open(Path(file), 'rb') as f:
                return IgInfoFactory(file.stem, json.load(f))
        raise FileNotFoundError('')

    @property
    def hashtag(self) -> str:
        # TODO check
        return re.split(r'\d', self._file, 1)[0]

    @property
    def extract_date(self) -> str:
        """TODO"""
        pass

    @property
    def download_date(self) -> str:
        """TODO"""
        pass

    def collect_latest_posts(self) -> LatestPostInfo:
        ret = []
        for it in self.data:
            lps = it['latestPosts']
            if len(lps) != 0:
                for lp in lps:  # type: PostDict
                    ret.append(lp)

        return LatestPostInfo(self.hashtag, ret)


def load_from_directory(d: Path | str) -> list[IgInfoFactory]:
    return [IgInfoFactory.load(f) for f in Path(d).glob('*.json')]


if __name__ == '__main__':
    d = '/Users/simon/code/ywspj/test_data'
    ify = load_from_directory(d)
    info = [it.collect_latest_posts() for it in ify]
    ret = LatestPostInfo.concat(info)
    # print(ret.contain_duplicated())
    # print(ret.to_dataframe())
    ret.download_image(output_dir='.')
