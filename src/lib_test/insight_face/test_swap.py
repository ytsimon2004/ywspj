import cv2
import insightface
from matplotlib import pyplot as plt


def swap_show(img_file_a,
              img_file_b,
              app,
              swapper,
              plot_before=True,
              plot_after=True):
    img_a = cv2.imread(img_file_a)
    img_b = cv2.imread(img_file_b)

    if plot_before:
        fig, ax = plt.subplots(1, 2, figsize=(10, 5))
        ax[0].imshow(img_a[:, :, ::-1])
        ax[0].axis('off')
        ax[1].imshow(img_b[:, :, ::-1])
        ax[1].axis('off')
        plt.show()

    face_a = app.get(img_a)[0]
    face_b = app.get(img_b)[0]

    _img_a = img_a.copy()
    _img_b = img_b.copy()

    if plot_after:
        _img_a = swapper.get(_img_a, face_a, face_b, paste_back=True)
        _img_b = swapper.get(_img_b, face_b, face_a, paste_back=True)
        fig, ax = plt.subplots(1, 2, figsize=(10, 5))
        ax[0].imshow(_img_a[:, :, ::-1])
        ax[0].axis('off')
        ax[1].imshow(_img_b[:, :, ::-1])
        ax[1].axis('off')
        plt.show()


def main():
    a = '/Users/simon/Desktop/KU Leuven/Doctoral_training/Sam_thesis/S__19791948.jpg'
    b = '/Users/simon/Desktop/KU Leuven/Doctoral_training/Sam_thesis/S__19808303.jpg'
    app = insightface.app.FaceAnalysis(name='buffalo_l')
    app.prepare(ctx_id=0, det_size=(640, 640))
    swapper = insightface.model_zoo.get_model('/Users/simon/code/ywspj/test_data/inswapper_128.onnx',
                                              download=False,
                                              download_zip=False)
    swap_show(a, b, app, swapper)


if __name__ == '__main__':
    main()
