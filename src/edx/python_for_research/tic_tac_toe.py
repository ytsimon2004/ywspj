import random
from typing import Tuple, List

import numpy as np


class TicTacToe:

    def __init__(self):
        self.board: np.ndarray = self._create_board()

    @staticmethod
    def _create_board() -> np.ndarray:
        return np.zeros((3, 3))

    def place(self, player: int, position: Tuple[int, int]):
        """
        place a marker on the board
        :param player: {1, 2}
        :param position: desired location to place the marker, len of tuple equal to 2
        :return:
        """
        if self.board[position] == 0:
            self.board[position] = player
        else:
            raise RuntimeError(f'{position} has already been occupied')

    def possibilities(self) -> List[Tuple]:
        """which positions are available to either player for placing their marker"""
        ret = np.where(self.board == 0)
        return list(zip(*ret))

    def random_place(self, player: int):
        """select an available board position at random and place a marker"""
        self.place(player, random.choice(self.possibilities()))

    def row_win(self, player: int) -> bool:
        """check whether either player has won the game in any row"""
        for i in range(3):
            if np.all(self.board[i] == player):
                # print(f'player{player} win the game in row{i}!')
                return True
        return False

    def col_win(self, player: int) -> bool:
        """check whether either player has won the game in any column"""
        for i in range(3):
            if np.all(self.board[:, i] == player):
                # print(f'player{player} win the game in column{i}!')
                return True
        return False

    def diag_win(self, player: int) -> bool:
        """check whether either player has won the game in any diagonal"""
        # main diagonal
        md = [self.board[i, i] == player for i in range(3)]
        # off diagonal
        od = [self.board[j, 2 - j] == player for j in range(3)]

        if np.all(md) or np.all(od):
            # print(f'player{player} win the game in diagonal!')
            return True

        return False

    def evaluate(self) -> int:
        """evaluate the winner player
        If one of them has won, return that player's number.
        If the board is full but no one has won, return -1. Otherwise, return 0
        """
        winner = 0
        for player in [1, 2]:
            rw = self.row_win(player)
            cw = self.col_win(player)
            dw = self.diag_win(player)
            if np.any([rw, cw, dw]):
                winner = player

        if np.all(self.board != 0) and winner == 0:
            winner = -1

        return winner

    def play_game(self) -> int:
        player = 1

        while self.evaluate() == 0:
            self.random_place(player)
            # switch player
            player = 2 if player == 1 else 1

        return self.evaluate()

    def reset(self):
        """reset the board"""
        self.board = self._create_board()



def place_three() -> np.ndarray:
    """both players place three markers each, for `HW2-Exercise5`"""
    random.seed(1)

    tt = TicTacToe()
    for i in range(3):
        tt.random_place(1)
        tt.random_place(2)

    return tt.board



if __name__ == '__main__':
    # ex5
    # ret = place_three()
    # print(ret)

    # ----
    # ex10
    random.seed(1)
    tt = TicTacToe()
    ret = []
    for i in range(1000):
        ret.append(tt.play_game())
        tt.reset()

    print(len([i for i in ret if i == 1]))   # winner 1
    print(len([i for i in ret if i == 2]))   # winner 2
    print(len([i for i in ret if i == -1]))

