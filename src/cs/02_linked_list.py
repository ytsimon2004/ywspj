"""
Refer to https://www.freecodecamp.org/news/introduction-to-linked-lists-in-python/
"""
from typing import Optional


class Node:

    def __init__(self, value: int):
        self.value = value
        self.next: Optional[Node] = None


class LinkedList:

    def __init__(self, head: Node):
        self.head = head

    def append(self, new_node: Node):
        cur = self.head

        if cur is not None:
            while cur.next:  # next is not None
                cur = cur.next
            cur.next = new_node
            print(cur.next.value)
        else:
            self.head = new_node

    def delete(self, value):
        """delete the first node with a given value"""
        cur = self.head
        if cur.value == value:
            self.head = cur.next
        else:
            while cur:
                if cur.value == value:
                    break

                prev = cur
                cur = cur.next

            if cur is None:
                return

            prev.next = cur.next
            cur = None


if __name__ == '__main__':
    head = Node(1)
    new_node = Node(2)

    ll = LinkedList(head)
    ll.append(new_node)



