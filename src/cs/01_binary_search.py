import unittest
from typing import List


class Solution:

    def binary_search(self, ls: List[int], item: int):
        n = len(ls)
        pointer = n // 2

        while True:
            if item == ls[pointer]:
                return pointer

            if (pointer + 1 == n or pointer == 0) and ls[pointer] != item:
                return None

            if item > ls[pointer]:
                pointer = pointer + (n - pointer) // 2  # right

            else:
                pointer = pointer - pointer // 2  # left



    def binary_search_ans(self, ls: List[int], item: int):
        low = 0
        high = len(ls) - 1

        while low <= high:
            mid = (low + high) // 2
            guess = ls[mid]

            if guess == item:
                return mid

            elif guess > item:
                high = mid - 1
            else:
                low = mid + 1


        return None



class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.solution = Solution()

    def testqual(self):
        self.assertEqual(self.solution.binary_search([1, 2, 3, 4, 5], 2), 1)
        self.assertEqual(self.solution.binary_search([1, 2, 3, 4, 5], 3), 2)
        self.assertEqual(self.solution.binary_search([5, 7, 10, 11, 100, 101], 101), 5)
        self.assertEqual(self.solution.binary_search([5, 7, 10, 11, 100, 101], 1000), None)


        self.assertEqual(self.solution.binary_search_ans([1, 2, 3, 4, 5], 2), 1)
        self.assertEqual(self.solution.binary_search_ans([1, 2, 3, 4, 5], 3), 2)
        self.assertEqual(self.solution.binary_search_ans([5, 7, 10, 11, 100, 101], 101), 5)
        self.assertEqual(self.solution.binary_search_ans([5, 7, 10, 11, 100, 101], 1000), None)

