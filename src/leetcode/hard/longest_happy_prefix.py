import unittest


class Solution:
    def longestPrefix(self, s: str) -> str:
        return self.longestPrefix_ta(s)

    def longestPrefix_yw(self, s: str) -> str:
        # Time Limit Exceeded
        ret = []
        for i in range(len(s)):
            if s[:i] == s[-i:]:
                ret.append(i)
        if len(ret) != 0:
            return s[:ret[-1]]
        else:
            return ''

    def longestPrefix_ta(self, s:str) -> str:
        length = len(s)

        if length <= 1:
            return ''

        if set(s) == 1:
            return s[:-1]

        longest = length - 1

        while longest > 0:
            left = longest - 1
            right = length - 1

            if s[left] == s[right]:
                while left >= 0 and s[left] == s[right]:
                    left -= 1
                    right -= 1

                if left < 0:
                    return s[:longest]

            longest -= 1
        return ''



class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.solution = Solution()

    def test_it(self):
        self.assertEqual('l', self.solution.longestPrefix('level'))
        self.assertEqual('abab', self.solution.longestPrefix('ababab'))
        self.assertEqual('', self.solution.longestPrefix(''))
        self.assertEqual('', self.solution.longestPrefix('a'))
        self.assertEqual('', self.solution.longestPrefix('abc'))
        self.assertEqual('a', self.solution.longestPrefix('aba'))
        self.assertEqual('aab', self.solution.longestPrefix('aabaab'))
        self.assertEqual('aaaaa', self.solution.longestPrefix('aaaaaa'))

    def test_perf(self):
        pass

if __name__ == '__main__':
    unittest.main()
