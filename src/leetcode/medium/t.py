class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        sign_a = 1 if dividend > 0 else 0
        sign_b = 1 if divisor > 0 else 0
        dividend = dividend if sign_a else -dividend
        divisor = divisor if sign_b else -divisor

        if divisor == dividend:
            ret = 1
        elif divisor == 1:
            ret = dividend
        elif divisor == 2:
            ret = dividend >> 1
        elif divisor == 4:
            ret = dividend >> 2
        else:
            ret = 0
            for i in range(31, -1, -1):
                a = divisor << i

                if dividend >= a:
                    dividend -= a
                    ret = (ret << 1) + 1
                else:
                    ret <<= 1


        if sign_a != sign_b:
            ret = -ret

        if ret >= 2147483647:
            return 2147483647
        elif ret <= -2147483648:
            return -2147483648

        return ret
import unittest
class TestS(unittest.TestCase):
    def setUp(self) -> None:
        self.s = Solution()

    def test(self):
        self.assertEqual(3, self.s.divide(10, 3))
        self.assertEqual(-2, self.s.divide(7, -3))
        self.assertEqual(1, self.s.divide(1, 1))
        self.assertEqual(-1, self.s.divide(10, -10))
        self.assertEqual(-1, self.s.divide(1, -1))
        self.assertEqual(2147483647, self.s.divide(-2147483648, -1))
        self.assertEqual(715827882, self.s.divide(2147483647, 3))
unittest.main()