import unittest


class Solution:

    def add_string(self, num1: str, num2: str) -> str:

        # idx
        i = len(num1) - 1
        j = len(num2) - 1

        ret = ''
        tmp = 0

        while i >= 0 or j >= 0 or tmp != 0:

            cur_i = int(num1[i]) if i >= 0 else 0
            cur_j = int(num2[j]) if j >= 0 else 0

            s = cur_i + cur_j + tmp
            tmp = 0

            if s >= 10:
                tmp += 1
                s = s % 10

            ret += str(s)

            i -= 1
            j -= 1

        return ret[::-1]


class Test(unittest.TestCase):

    def setUp(self) -> None:
        self.solution = Solution()

    def test(self):
        self.assertEqual(self.solution.add_string("11", "123"), '134')
        self.assertEqual(self.solution.add_string("456", "77"), '533')
        self.assertEqual(self.solution.add_string("0", "0"), '0')
        self.assertEqual(self.solution.add_string("1", "9"), '10')
