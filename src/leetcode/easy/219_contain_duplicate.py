import unittest
from typing import List


class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        dy = {}
        for i, it in enumerate(nums):
            if it not in dy:
                dy[it] = i
            else:
                cmp = i - dy[it]
                if cmp <= k:
                    return True
                dy[it] = i

        return False



class Test(unittest.TestCase):

    def setUp(self):
        self.solution = Solution()

    def test(self):
        self.assertTrue(self.solution.containsNearbyDuplicate([1, 2, 3, 1], 3))
        self.assertTrue(self.solution.containsNearbyDuplicate([1, 0, 1, 1], 1))
        self.assertFalse(self.solution.containsNearbyDuplicate([1, 2, 3, 1, 2, 3], 2))
