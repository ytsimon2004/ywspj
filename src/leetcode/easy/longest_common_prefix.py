from typing import List


def longestCommonPrefix(strs: List[str]) -> str:

    if len(strs) == 0 or len(strs) >= 200:
        raise ValueError(f'{strs} length is not fit')

    for i, s in enumerate(strs):
        if len(s) == 0:
            break
        if len(s) >= 200:
            raise ValueError(f'{s} length is not fit')
        if not s.islower():
            raise ValueError(f'{s} should be consist of all lowercase')

    current = strs[0]
    for i in range(1, len(strs)):
        tmp = ''

        if len(current) == 0:
            break

        for j in range(len(strs[i])):
            if j + 1 < len(current) and strs[i][j] == current[j]:
                tmp += strs[i][j]
            else:
                break

        current = tmp

    return current


class Solution(object):
    pass

assert longestCommonPrefix(["school", "schedule", "scotland"]) == 'sc'
assert longestCommonPrefix(["flower", "flow", "flight"]) == 'fl'
assert longestCommonPrefix(["dog", "racecar", "car"]) == ''
