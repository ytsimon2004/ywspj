import unittest


class Solution:
    def climbStairs(self, n: int) -> int:

        if n == 1 or n == 2:
            return n

        while True:
            return self.climbStairs(n - 1) + self.climbStairs(n - 2)

    def climbStairs_for(self, n: int) -> int:

        one, two = 1, 1

        for i in range(n-1):
            tmp = one
            one = one + two
            two = tmp

        return one



class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.solution = Solution()

    def test_it(self):
        # self.assertEqual(2, self.solution.climbStairs(2))
        # self.assertEqual(3, self.solution.climbStairs(3))
        # self.assertEqual(5, self.solution.climbStairs(4))
        # self.assertEqual(8, self.solution.climbStairs(5))

        self.assertEqual(2, self.solution.climbStairs_for(2))
        self.assertEqual(3, self.solution.climbStairs_for(3))
        self.assertEqual(8, self.solution.climbStairs_for(5))



if __name__ == '__main__':
    unittest.main()
