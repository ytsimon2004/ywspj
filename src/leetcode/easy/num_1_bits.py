class Solution:
    def hammingWeight(self, n: int) -> int:
        s = str(bin(n))
        nb = 0
        for it in s:
            if it == '1':
                nb+= 1

        return nb

