from collections import Counter
from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        c = Counter(nums)
        for k, v in c.items():
            if v == 1:
                return k


if __name__ == '__main__':
    nums = [4, 1, 2, 1, 2, 5]
    ret = Solution().singleNumber(nums)
    print(ret)
