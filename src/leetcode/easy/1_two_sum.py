import unittest
from typing import List


class Solution:

    def two_sum_simple(self, ls: List[int], target: int) -> List[int]:
        for i, it1 in enumerate(ls):
            for j, it2 in enumerate(ls):
                if i != j and it1 + it2 == target:
                    return [i, j]

    # faster
    def two_sum(self, ls: List[int], target: int) -> List[int]:

        position = {}  # val:idx
        for i, it in enumerate(ls):
            want = target - it
            if want in position:
                return [position[want], i]
            else:
                position[it] = i


class Test(unittest.TestCase):

    def setUp(self) -> None:
        self.solution = Solution()

    def test(self):
        #
        self.assertEqual(self.solution.two_sum_simple([2, 7, 11, 15], 9), [0, 1])
        self.assertEqual(self.solution.two_sum_simple([3, 2, 4], 6), [1, 2])
        self.assertEqual(self.solution.two_sum_simple([3, 3], 6), [0, 1])
        #
        self.assertEqual(self.solution.two_sum([2, 7, 11, 15], 9), [0, 1])
        self.assertEqual(self.solution.two_sum([3, 2, 4], 6), [1, 2])
        self.assertEqual(self.solution.two_sum([3, 3], 6), [0, 1])
