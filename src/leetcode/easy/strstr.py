class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if needle not in haystack:
            return -1
        else:
            return haystack.find(needle)

    def _strStr_nb(self, haystack: str, needle: str) -> int:
        if needle not in haystack:
            return -1
        else:
            return haystack.find(needle)  # TODO not allowed to use built-in?





if __name__ == '__main__':
    haystack = "mississippi"
    needle = "issip"

    ret = Solution().strStr(haystack, needle)
    print(ret)

# "mississippi"
# "issip"


