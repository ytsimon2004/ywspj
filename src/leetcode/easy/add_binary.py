
class Solution:
    def addBinary(self, a: str, b: str) -> str:
        _a = int(a, 2)
        _b = int(b, 2)
        return format(_a+_b, 'b')




if __name__ == '__main__':
    a = '1010'
    b = '1011'
    print(Solution().addBinary(a, b))
