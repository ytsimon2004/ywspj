import unittest


class Solution:
    def isUgly(self, n: int) -> bool:
        """An ugly number is a positive integer whose prime factors are limited to 2, 3, and 5"""
        if n <= 0:
            return False

        def dev(dividend: int, divisor: int):
            while dividend % divisor == 0:
                dividend //= divisor
            return dividend

        for factor in [2, 3, 5]:
            n = dev(n, factor)
            print(n)

        return n == 1

class Test(unittest.TestCase):

    def setUp(self):
        self.solution = Solution()

    def test(self):
        self.assertTrue(self.solution.isUgly(6))
        # self.assertTrue(self.solution.isUgly(1))
        # self.assertFalse(self.solution.isUgly(14))
