from typing import List

# TODO NOT YET DONE
class Solution:

    def removeDuplicates3(self, nums: List[int]) -> int:
        dup = []
        for i, n in enumerate(nums):
            if n not in dup:
                dup.append(n)
            if n in dup:
                nums.remove(n)

        print(nums)
        return len(dup)

#
if __name__ == '__main__':
    nums = [1, 1, 1, 1]
    ret = Solution().removeDuplicates3(nums)
    print(ret)
