import unittest


class Solution:

    def convertToTitle(self, columnNumber: int) -> str:
        ret = ''
        while columnNumber:
            columnNumber, mod = divmod(columnNumber - 1, 26)
            ret += chr(mod + 65)

        return ret[::-1]


class Test(unittest.TestCase):

    def setUp(self):
        self.solution = Solution()

    def test(self):
        self.assertEqual(self.solution.convertToTitle(1), 'A')
        self.assertEqual(self.solution.convertToTitle(28), 'AB')
        self.assertEqual(self.solution.convertToTitle(701), 'ZY')
