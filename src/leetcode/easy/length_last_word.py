import unittest


class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        s = s.strip()
        i = s.rfind(' ')
        return len(s[i+1:])





class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.solution = Solution()


    def test_it(self):
        self.assertEqual(4, self.solution.lengthOfLastWord("   fly me   to   the moon  "))
        self.assertEqual(6, self.solution.lengthOfLastWord("luffy is still joyboy"))
        self.assertEqual(5, self.solution.lengthOfLastWord("Hello World"))


if __name__ == '__main__':
    unittest.main()
