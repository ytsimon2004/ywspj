import unittest
from typing import List


class Solution:

    def contain_duplicate(self, ls: List[int]) -> bool:
        s = set(ls)
        if len(s) == len(ls):
            return True
        else:
            return False


class Test(unittest.TestCase):

    def setUp(self) -> None:
        self.solution = Solution()

    def test_true(self):
        self.assertTrue(self.solution.contain_duplicate([1, 2, 3, 4]))
        self.assertFalse(self.solution.contain_duplicate([1, 2, 3, 1]))
        self.assertFalse(self.solution.contain_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]))
