from typing import List


class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        fold = len(digits) - 1
        plus = 1
        for i, v in enumerate(digits):
            plus = plus + (10 ** fold * v)
            fold -= 1

        return [int(s) for s in str(plus)]


if __name__ == '__main__':
    digits = [9]
    ret = Solution().plusOne(digits)
    print(ret)
