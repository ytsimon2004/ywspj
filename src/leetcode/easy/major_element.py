from collections import Counter
from typing import List


class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        dy = {}
        counts = 0
        for it in nums:
            dy[it] = dy.get(it, counts)
            dy[it] = dy.get(it) + 1

        # print(dy)
        max_idx = list(dy.values()).index(max(dy.values()))

        return list(dy.keys())[max_idx]


if __name__ == '__main__':
    n = [2, 2, 1, 1, 1, 2, 2]
    ret = Solution().majorityElement(n)
    print(ret)
