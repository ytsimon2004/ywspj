from typing import Optional, List


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class LinkList:
    def __init__(self):
        self.head = None

    def append(self, new_val):
        """push new value to linked list"""
        new_node = ListNode(new_val)

        # if head is None, initialize it to new node
        if self.head is None:
            self.head = new_node
            return

        cur_node = self.head
        while cur_node.next is not None:
            cur_node = cur_node.next

    def sortedmerge(self, a: Optional[ListNode], b: Optional[ListNode]) -> Optional[ListNode]:
        ret = None
        if a is None:
            return b
        if b is None:
            return a

        if a.val <= b.val:
            ret = a
            ret.next = self.sortedmerge(a.next, b)
        else:
            ret = ret.next = self.sortedmerge(a, b.next)

        return ret

    def mergesort(self, h: Optional[ListNode]):
        if h is None or h.next is None:
            return h
        #todo

    def get_middle(self, head):
        """to get the middle of the link list"""
        if head is None:
            return head

        slow = head
        fast = head

        while fast.next is not None and fast.next.next is not None:
            slow = slow.next
            fast = fast.next.next

        return slow



# if __name__ == '__main__':
#     l1 = ListNode(val=[1,2,4])
