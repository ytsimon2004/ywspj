class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        d1 = {}
        d2 = {}
        for i in range(len(s)):
            d1[s[i]] = t[i]
            d2[t[i]] = s[i]

        print(d1, d2)
        return list(d1.keys()) == list(d2.values())



if __name__ == '__main__':
    # s = "egg"
    # t = "add"
    s = "paper"
    t = "title"
    ret = Solution().isIsomorphic(s,t)
    print(ret)