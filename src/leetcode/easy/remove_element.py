from typing import List


class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        if val in nums:
            counts = nums.count(val)
            for _ in range(counts):
                nums.remove(val)

        return len(nums)


if __name__ == '__main__':
    s = Solution()
    ret = s.removeElement([0, 1, 2, 2, 3, 0, 4, 2], val=2)
    print(ret)
