from typing import List


class Solution:
    def generate(self, numRows: int) -> List[List[int]]:
        ret = []
        for n in range(numRows):
            ret.append([])
            ret[n].append(1)
            print('n', ret)
            for m in range(1, n):
                left = ret[n - 1][m - 1]
                right = ret[n - 1][m]
                ret[n].append(left + right)
                print('m', ret)

            if numRows != 0:
                ret[n].append(1)

        ret[0].pop(1)
        return ret


if __name__ == '__main__':
    ret = Solution().generate(5)
    print(ret)
