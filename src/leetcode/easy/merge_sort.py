import unittest
from typing import List


class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        last_idx = m + n - 1

        # merge in a reverse order
        while m > 0 and n > 0:
            if nums1[m - 1] > nums2[n - 1]:
                nums1[last_idx] = nums1[m - 1]
                m -= 1
            else:
                nums1[last_idx] = nums2[n - 1]
                n -= 1

            last_idx -= 1

        # fill nums1 with leftover nums2 elements
        while n > 0:
            nums1[last_idx] = nums2[n - 1]
            n -= 1
            last_idx -= 1


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.solution = Solution()

    def test_it(self):
        self.assertEqual([1, 2, 2, 3, 5, 6], self.solution.merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3))


if __name__ == '__main__':
    unittest.main()
