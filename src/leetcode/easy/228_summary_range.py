import unittest
from typing import List


class Solution:

    def summary_range(self, ls: List[int]) -> List[str]:
        ret = []
        for it in ls:
            if ret and ret[-1][1] == it - 1:
                ret[-1][1] = it
            else:
                ret.append([it, it])

        return [str(l) + '->' + str(r) if l != r else str(l) for (l, r) in ret]

class Test(unittest.TestCase):

    def setUp(self) -> None:
        self.solution = Solution()

    def test(self):
        self.assertEqual(self.solution.summary_range([0, 1, 2, 4, 5, 7]),
                         ["0->2", "4->5", "7"])
        self.assertEqual(self.solution.summary_range([0, 2, 3, 4, 6, 8, 9]),
                         ["0", "2->4", "6", "8->9"])
