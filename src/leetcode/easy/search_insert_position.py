from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        nums = sorted(nums)
        if target in nums:
            return nums.index(target)

        else:
            nums.append(target)
            nums.sort()
            return nums.index(target)


if __name__ == '__main__':
    s = Solution()
    ret = s.searchInsert(nums=[1,3,5,6], target=7)
    print(ret)


# [2, 1, 2, 3, 5]
