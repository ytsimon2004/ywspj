import string
import unittest


class Solution:
    def titleToNumber(self, columnTitle: str) -> int:
        ret = 0
        dy = {
            up: i + 1
            for i, up in enumerate(list(string.ascii_uppercase))
        }

        fold = len(dy)

        for i, c in enumerate(columnTitle[::-1]):
            if i == 0:
                ret = ret + dy[c] * (i + 1)
            else:
                ret = ret + dy[c] * fold ** i


        return ret


# class Test(unittest.TestCase):
#
#     def setUp(self) -> None:
#         self.solution = Solution()
#
#     def test(self):
#         self.assertEqual(1, self.solution.titleToNumber('A'))
#         self.assertEqual(28, self.solution.titleToNumber('AB'))
#         self.assertEqual(701, self.solution.titleToNumber('ZY'))
#         self.assertEqual(2147483647, self.solution.titleToNumber("FXSHRXW"))


if __name__ == '__main__':
    # unittest.main()
    c = 'BCD'
    ret = Solution().titleToNumber(c)
    print(ret)