from typing import List


class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        ret = []
        nums = list(map(int, nums))
        for i, it in enumerate(zip(nums[:-1], nums[1:])):
            if it[1] - it[0] == 1:
                ret.append(f'{it[0]}->{it[1]}')
            else:
                ret.append(f'{it[0]}')

        ret2 = []
        for i, it2 in enumerate(ret):
            ll = it2.split('->')

            if i < len(ret) - 1:
                if ll[-1] == ret[i + 1][0]:
                    ret2.append(f'{ll[0]}->{ret[i + 1][-1]}')
                else:
                    pass

        return ret2

    def summaryRanges_ans(self, nums: List[int]) -> List[str]:
        """
        Input: nums = [0,2,3,4,6,8,9]
        Output: ["0","2->4","6","8->9"]

        :param nums:
        :return:
        """

        output = []
        start = nums[0]
        cur = nums[0]
        end = None

        for n in nums[1:]:
            cur += 1
            if n == cur:
                end = cur
            else:
                if not end:
                    output.append(f'{start}')
                else:
                    output.append(f'{start}->{end}')

                start = n
                cur = n
                end = None


        return output

if __name__ == '__main__':
    nums = [0, 2, 3, 4, 6, 8, 9]
    ret = Solution().summaryRanges(nums)
    print(ret)
