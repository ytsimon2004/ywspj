import unittest
from typing import List


class Solution:

    def plus_one(self, digits: List[int]) -> List[int]:
        fold = len(digits) - 1
        plus = 1
        for i, it in enumerate(digits):
            plus = plus + (10 ** fold * it)
            fold -= 1

        return [int(s) for s in str(plus)]


class Test(unittest.TestCase):

    def setUp(self):
        self.solution = Solution()

    def test(self):
        self.assertEqual(self.solution.plus_one([1, 2, 3]), [1, 2, 4])
        self.assertEqual(self.solution.plus_one([4, 3, 2, 1]), [4, 3, 2, 2])
        self.assertEqual(self.solution.plus_one([9]), [1, 0])
        self.assertEqual(self.solution.plus_one([1, 9]), [2, 0])
        self.assertEqual(self.solution.plus_one([9, 9]), [1, 0, 0])
