import unittest
from typing import List


class Solution:

    def is_happy(self, num: int) -> bool:
        """FIXME time exceed"""
        num = str(num)

        while int(num) >= 10:
            ret = 0
            for n in num:
                s = int(n) ** 2
                ret += s

            if ret == 1:
                return True

            num = str(ret)

        return False

    def is_happy_ans(self, num: int) -> bool:
        ret = []
        while True:
            sum = 0
            for it in str(num):
                sum = sum + int(it) * int(it)

            num = sum
            if num in ret:
                return False
            ret.append(num)
            if num == 1:
                return True


class Test(unittest.TestCase):

    def setUp(self) -> None:
        self.solution = Solution()

    def test(self):
        #
        self.assertTrue(self.solution.is_happy(19))
        self.assertFalse(self.solution.is_happy(2))
        #
        self.assertTrue(self.solution.is_happy_ans(19))
        self.assertFalse(self.solution.is_happy_ans(2))
