# def isValid(s: str) -> bool:
#     d = {'(': ')',
#          '{': '}',
#          '[': ']'}
#
#     if s.endswith(tuple(d.keys())):
#         return False
#
#     # if len(s) % 2 != 0:
#     #     return False
#
#     for k, v in d.items():
#         if k in s and v in s:
#             for i, ss in enumerate(s):
#                 if ss == k and s[i + 1] == v:
#                     return True
#
#     return False


def isValid(s: str) -> bool:
    while True:
        if '()' in s:
            s = s.replace('()', '')
        elif '[]' in s:
            s = s.replace('[]', '')
        elif '{}' in s:
            s = s.replace('{}', '')

        else:
            return not s  # len(s) == 0


assert isValid("{[]}") == True
assert isValid("()[]{}") == True
assert isValid("([)]") == False
assert isValid("(){}}{") == False
assert isValid("({{{{}}}))") == False
