import os
import webbrowser
from pathlib import Path
from typing import List

import openai


class ImgCreate:

    def __init__(self,
                 description: str,
                 number: int = 1,
                 size: str = '512x512'):
        """
        OpenAI-based image create

        :param description: text msg for the image description
        :param number: how many number of url produced
        :param size: resolution
        """
        self._try_token()

        self.description = description
        self.number = number
        self.size = size

    @classmethod
    def _try_token(cls):
        try:
            from dotenv import load_dotenv
            env_path = Path(__file__).parent / '.env'
            load_dotenv(dotenv_path=env_path)
            try:
                k = os.environ['OPENAI_TOKEN']
            except KeyError:
                raise RuntimeError('OPENAI_TOKEN not found in the env file')
            else:
                openai.api_key = k
        except BaseException:
            raise RuntimeError('env path not set properly')

    def open(self, n=0):
        img = openai.Image.create(prompt=self.description,
                                  n=self.number,
                                  size=self.size)
        try:
            url = img['data'][n]['url']
        except IndexError as e:
            print(f'{n} exceed, increase number while init')
            raise e
        else:
            return webbrowser.open(f'{url}')


def main(arg: List[str] = None):
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('-D', '--desc', nargs='+',
                    required=True, help='image description')
    opt = ap.parse_args(arg)
    desc = ' '.join(opt.desc)
    img = ImgCreate(desc)
    img.open()


if __name__ == '__main__':
    main()
